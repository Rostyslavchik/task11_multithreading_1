package com.rostyslavprotsiv;

import com.rostyslavprotsiv.controller.Controller;

public class Main {
    public static void main(String[] args) {
        Controller controller = new Controller();
        controller.execute(Integer.parseInt(args[0]));
    }
}

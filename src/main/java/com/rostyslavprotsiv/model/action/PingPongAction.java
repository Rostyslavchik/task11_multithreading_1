package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.pingpongtask.PingThread;
import com.rostyslavprotsiv.model.entity.pingpongtask.PongRunnable;

public class PingPongAction {

    public void playPingPong() {
        PingThread pingThread = new PingThread();
        PongRunnable pongRunnable = new PongRunnable();
        Thread pongThread = new Thread(pongRunnable);
        pongThread.start();
        pingThread.start();
    }
}

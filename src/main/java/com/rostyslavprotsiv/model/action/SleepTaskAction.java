package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.task.SleepTask;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SleepTaskAction {

    public void doSleepTasks(int numOfTasks) {
//        List<SleepTask> tasks = Stream.generate(SleepTask::new)
//                .limit(numOfTasks)
//                .collect(Collectors.toList());
        if (numOfTasks <= 0) {
            throw new IllegalArgumentException("Number of tasks should be > 0");
        }
        ScheduledExecutorService service = Executors.newScheduledThreadPool(3);
        ScheduledExecutorService service1 = Executors.newSingleThreadScheduledExecutor();
        Runnable task = new SleepTask();
//        for (int i = 0; i < numOfTasks; i ++) {
//            service.schedule(task, 3, TimeUnit.SECONDS);
//        }
//        service.shutdown();
//        for (int i = 0; i < numOfTasks; i ++) {
//            service1.schedule(task, 3, TimeUnit.SECONDS);
//        }
//        service1.shutdown();
//        service1.scheduleAtFixedRate(task, 3, 3, TimeUnit.SECONDS); // every 3 seconds it calls run()
        service1.scheduleWithFixedDelay(task, 3, 3, TimeUnit.SECONDS); // every 3 seconds at the end of run() calls next run()
//        service.shutdown();
    }
}

package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.task.CallableTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.*;

public class CallableTaskAction {
    private static final Logger LOGGER = LogManager
            .getLogger(CallableTaskAction.class);

    public void doCallables() {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        ExecutorService executorService1 = Executors.newFixedThreadPool(3);
        ExecutorService executorService2 = Executors.newWorkStealingPool();
        Callable<Integer> callable = new CallableTask(5);
        Callable<Integer> callable1 = new CallableTask(10);
        Callable<Integer> callable2 = new CallableTask(1);
        List<Callable<Integer>> callables = Arrays.asList(callable,
                callable1, callable2, callable);
//        Future<Integer> future = executorService.submit(callable);
//        Future<Integer> future1 = executorService.submit(callable1);
//        Future<Integer> future2 = executorService.submit(callable2);
//        try {
//            LOGGER.info(future.get());
//            LOGGER.info(future1.get());
//            LOGGER.info(future2.get());
//        } catch (InterruptedException | ExecutionException e) {
//            e.printStackTrace();
//        }
//        executorService.shutdown();


//        Future<Integer> future = executorService1.submit(callable);
//        Future<Integer> future1 = executorService1.submit(callable1);
//        Future<Integer> future2 = executorService1.submit(callable2);
//        try {
//            LOGGER.info(future.get());
//            LOGGER.info(future1.get());
//            LOGGER.info(future2.get());
//        } catch (InterruptedException | ExecutionException e) {
//            e.printStackTrace();
//        }
//        executorService1.shutdown();

        try {
            executorService2.invokeAll(callables).forEach(future -> {
                try {
                    LOGGER.info(future.get());
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

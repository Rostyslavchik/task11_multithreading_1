package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.pipetask.PipeReader;
import com.rostyslavprotsiv.model.entity.pipetask.PipeWriter;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class PipesCommunication {

    public String testCommunication() {
        PipedOutputStream pipedOutputStream = new PipedOutputStream();
        PipedInputStream pipedInputStream = new PipedInputStream();
        PipeWriter writer = new PipeWriter(pipedOutputStream, "Abalavaa");
        PipeReader reader = new PipeReader(pipedInputStream);
        String result = null;
        try {
            pipedInputStream.connect(pipedOutputStream);//test it
//            pipedOutputStream.connect(pipedInputStream);//It works also
            ExecutorService service = Executors.newFixedThreadPool(2);
            service.submit(writer);
            Future<String> future = service.submit(reader);
            result = future.get();
            service.shutdown();
        } catch (IOException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return result;
    }
}

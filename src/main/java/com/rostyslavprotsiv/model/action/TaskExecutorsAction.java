package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.task.Task;

import java.util.concurrent.*;

public class TaskExecutorsAction {
    public void doTasks() {
        Task task1 = new Task(5);
        Task task2 = new Task(10);
        Task task3 = new Task(1);
        ExecutorService executor = Executors.newSingleThreadExecutor();
        ExecutorService executor1 = Executors.newFixedThreadPool(2);
        ExecutorService executor2 = Executors.newCachedThreadPool();
        ExecutorService executor3 = Executors.newWorkStealingPool();
        ScheduledExecutorService executor4 = Executors.newSingleThreadScheduledExecutor();

//        executor.execute(task1);
//        executor.execute(task2);
//        executor.execute(task3);
//        executor.execute(task1);
//        executor.shutdown();


//        try {
//            executor1.execute(task1);
//            executor1.awaitTermination(1000, TimeUnit.MILLISECONDS);
//            executor1.execute(task2);
//            executor1.awaitTermination(1000, TimeUnit.MILLISECONDS);
//            executor1.execute(task3);
//            executor1.awaitTermination(1000, TimeUnit.MILLISECONDS);
//            executor1.execute(task1);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        executor1.execute(task1);
//        executor1.execute(task2);
//        executor1.execute(task3);
//        executor1.execute(task1);
//        executor1.shutdown();

//        executor2.execute(task1);
//        executor2.execute(task2);
//        executor2.execute(task3);
//        executor2.execute(task1);
//        executor2.shutdown();

//        executor3.execute(task1);
//        executor3.execute(task2);
//        executor3.execute(task3);
//        executor3.execute(task1);
//        executor3.shutdown();

//        executor4.schedule(task2, 4, TimeUnit.SECONDS);
        executor4.scheduleAtFixedRate(task2, 3, 3,  TimeUnit.SECONDS);
//        executor4.scheduleWithFixedDelay(task2, 3, 3,  TimeUnit.SECONDS);
//        executor4.shutdown();
    }
}

package com.rostyslavprotsiv.model.action;

public class SynchronizedAction {
    private int counter = 0;
    private static final Object MON = new Object();

    public int getCounter() {
        return counter;
    }

    public void incrC1() {
        synchronized (MON) {
            for (int i = 0; i < 100000; i ++) {
                counter++;
            }
        }
    }

    public void incrC2() {
        synchronized (MON) {
            for (int i = 0; i < 100000; i ++) {
                counter++;
            }
        }
    }

    public void incrC3() {
        synchronized (MON) {
            for (int i = 0; i < 100000; i ++) {
                counter++;
            }
        }
    }

    public void incrC4() {
        Object badMon = new Object();
        synchronized (badMon) {
            for (int i = 0; i < 100000; i ++) {
                counter++;
            }
        }
    }

    public void incrC5() {
        Object badMon = new Object();
        synchronized (badMon) {
            for (int i = 0; i < 100000; i ++) {
                counter++;
            }
        }
    }

    public void incrC6() {
        Object badMon = new Object();
        synchronized (badMon) {
            for (int i = 0; i < 100000; i ++) {
                counter++;
            }
        }
    }
}

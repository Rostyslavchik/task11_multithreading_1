package com.rostyslavprotsiv.model.action;

import com.rostyslavprotsiv.model.entity.task.Task;

public class TaskAction {

    public void doTasks() {
        Task task1 = new Task(5);
        Task task2 = new Task(10);
        Task task3 = new Task(1);
        Thread thread1 = new Thread(task1);
        Thread thread2 = new Thread(task2);
        Thread thread3 = new Thread(task3);
        Thread thread4 = new Thread(task1);
        try {
            thread1.start();
            thread1.join();
            thread2.start();
            thread2.join();
            thread3.start();
            thread3.join();
            thread4.start();
            thread4.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//            thread1.start();
//            thread2.start();
//            thread3.start();
//            thread4.start();
    }
}

package com.rostyslavprotsiv.model.entity.pipetask;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class PipeReader implements Callable<String> {
    private PipedInputStream pipedInputStream;

    public PipeReader(PipedInputStream pipedInputStream) {
        this.pipedInputStream = pipedInputStream;
    }

    @Override
    public String call() {
        StringBuilder newMessage = new StringBuilder();
        try {
            int data = pipedInputStream.read();
            while (data != -1) {
                newMessage.append((char) data);
                System.out.println(newMessage.toString());
                data = pipedInputStream.read();
                System.out.println(Thread.currentThread().getName() + " is reading");
            }
            TimeUnit.SECONDS.sleep(4);
            pipedInputStream.close();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return newMessage.toString();
    }
}

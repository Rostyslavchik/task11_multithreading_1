package com.rostyslavprotsiv.model.entity.pipetask;

import java.io.IOException;
import java.io.PipedOutputStream;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

public class PipeWriter implements Runnable {
    private PipedOutputStream pipedOutputStream;
    private String message;

    public PipeWriter(PipedOutputStream pipedOutputStream, String message) {
        this.pipedOutputStream = pipedOutputStream;
        this.message = message;
    }

    @Override
    public void run() {
        try {
            pipedOutputStream.write(message.getBytes());
            System.out.println(Thread.currentThread().getName() + " is writing");
            pipedOutputStream.close(); // Important to close!!
            TimeUnit.SECONDS.sleep(4);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}

package com.rostyslavprotsiv.model.entity.pingpongtask;

public class PingThread extends Thread {

    @Override
    public void run() {
        try {
            PingPong.doPing();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

package com.rostyslavprotsiv.model.entity.pingpongtask;

public class PongRunnable implements Runnable {
    @Override
    public void run() {
        try {
            PingPong.doPong();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}

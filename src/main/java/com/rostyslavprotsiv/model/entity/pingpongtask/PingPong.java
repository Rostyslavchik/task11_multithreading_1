package com.rostyslavprotsiv.model.entity.pingpongtask;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPong {
    private static final Logger LOGGER = LogManager.getLogger(PingPong.class);

    public static synchronized void doPing() throws InterruptedException {
        while (true) {
            System.out.println(Thread.currentThread().getName() + ": - ping");
            PingPong.class.notify();
            PingPong.class.wait();
        }
    }

    public static synchronized void doPong() throws InterruptedException {
        while (true) {
            PingPong.class.wait();
            System.out.println(Thread.currentThread().getName() + ": - pong");
            PingPong.class.notify();
        }
    }
}

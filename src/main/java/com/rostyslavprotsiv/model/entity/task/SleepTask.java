package com.rostyslavprotsiv.model.entity.task;

import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class SleepTask implements Runnable {
    private static final int MIN_SECONDS = 1;
    private static final int MAX_SECONDS = 10;
    private static final Random RANDOM = new Random();

    @Override
    public void run() {
        Formatter f = new Formatter();
        Calendar cal = Calendar.getInstance();
        f.format("%tT:%tL", cal, cal);
        System.out.println(f);// out hours minutes seconds
        int sleepTime = RANDOM.nextInt(MAX_SECONDS - MIN_SECONDS)
                + MIN_SECONDS;
        try {
            TimeUnit.SECONDS.sleep(sleepTime);
//            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(Thread.currentThread().getName()
                + ", I was sleeping for : " + sleepTime);
    }
}

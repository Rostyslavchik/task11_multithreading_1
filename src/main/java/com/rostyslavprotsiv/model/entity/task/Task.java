package com.rostyslavprotsiv.model.entity.task;

public class Task implements Runnable {
//    private static Object monitor = new Object();
    private int n;

    public Task(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    @Override
    public void run() {
//        synchronized (Task.class) {
//        synchronized (monitor) {
            int previous = 0;
            int current = 1;
            int temp;
            System.out.print(Thread.currentThread().getName() + ": " + previous
                    + " " + current + " ");
            for (int i = 2; i < n; i++) {
                temp = previous;
                previous = current;
                current += temp;
                System.out.print(current + " ");
            }
            System.out.println();
        }
//    }
}

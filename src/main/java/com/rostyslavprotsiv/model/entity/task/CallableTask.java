package com.rostyslavprotsiv.model.entity.task;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

public class CallableTask implements Callable<Integer> {
    private int n;

    public CallableTask(int n) {
        this.n = n;
    }

    public int getN() {
        return n;
    }

    public void setN(int n) {
        this.n = n;
    }

    @Override
    public Integer call() {
        int sum = 1;
        int previous = 0;
        int current = 1;
        int temp;
//        try {
//            TimeUnit.SECONDS.sleep(5);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        System.out.print(Thread.currentThread().getName() + " ");
        for (int i = 2; i < n; i++) {
            temp = previous;
            previous = current;
            current += temp;
            sum += current;
        }
        return sum;
    }
}

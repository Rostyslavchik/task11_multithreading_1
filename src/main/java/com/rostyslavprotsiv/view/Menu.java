package com.rostyslavprotsiv.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Menu {
    private static final Logger LOGGER = LogManager.getLogger(Menu.class);

    public void welcome() {
        LOGGER.info("Hi!! Welcome to my program!!");
    }

    public void outPingPong() {
        LOGGER.info("Here is ping-pong demonstration: ");
    }

    public void outFibonacciTasks() {
        LOGGER.info("Here is Fibonacci tasks demonstration: ");
    }

    public void outFibonacciExecutorsTasks() {
        LOGGER.info("Here is Fibonacci tasks demonstration with Executors: ");
    }

    public void outFibonacciExecutorsCallableTasks() {
        LOGGER.info("Here is Fibonacci tasks demonstration with "
                + "Callable and Executors: ");
    }

    public void outSleepTask() {
        LOGGER.info("The execution result of sleep task:");
    }

    public void outTestWithMon(int counter) {
        LOGGER.info("Out testing result with monitor: " + counter);
    }

    public void outTestWithoutMon(int counter) {
        LOGGER.info("Out testing result without monitor: " + counter);
    }

    public void outPipeCommunicationResult(String res) {
        LOGGER.info("Out pipe-communication result: " + res);
    }
}

package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.action.*;
import com.rostyslavprotsiv.view.Menu;

public class Controller {
    private static final Menu MENU = new Menu();
    private static final PingPongAction PING_PONG_ACTION = new PingPongAction();
    private static final TaskAction TASK_ACTION = new TaskAction();
    private static final TaskExecutorsAction TASK_EXECUTORS_ACTION
            = new TaskExecutorsAction();
    private static final CallableTaskAction CALLABLE_TASK_ACTION
            = new CallableTaskAction();
    private static final SleepTaskAction SLEEP_TASK_ACTION
            = new SleepTaskAction();
    private static final SyncActionMon SYNC_ACTION_MON
            = new SyncActionMon();
    private static final PipesCommunication PIPES_COMMUNICATION
            = new PipesCommunication();

    public void execute(int numOfTasks) {
        MENU.welcome();
//        MENU.outPingPong();
//        PING_PONG_ACTION.playPingPong();
//        MENU.outFibonacciTasks();
//        TASK_ACTION.doTasks();
//        MENU.outFibonacciExecutorsTasks();
//        TASK_EXECUTORS_ACTION.doTasks();
//        MENU.outFibonacciExecutorsCallableTasks();
//        CALLABLE_TASK_ACTION.doCallables();
//        MENU.outSleepTask();
//        SLEEP_TASK_ACTION.doSleepTasks(numOfTasks);
//        MENU.outTestWithMon(SYNC_ACTION_MON.testWithMonitor());
//        MENU.outTestWithoutMon(SYNC_ACTION_MON.testWithoutMonitor());
        MENU.outPipeCommunicationResult(PIPES_COMMUNICATION.testCommunication());
    }
}

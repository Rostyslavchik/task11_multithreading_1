package com.rostyslavprotsiv.model.action;

import org.junit.jupiter.api.Test;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.*;

class SyncActionMonTest {
    private static SyncActionMon syncActionMon = new SyncActionMon();

    @Test
    void testTestAllMethods() {
        assertEquals(syncActionMon.testWithMonitor(), 300000);
        assertNotEquals(syncActionMon.testWithoutMonitor(), 300000);
    }

}